#!/usr/bin/env python

"""
Copyright (C) 2022 Edwin Saji Uthuppan (batmanfeynman@gmail.com)

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>. 
"""


import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button


def make_matrix( N=10, k=1.0 , m=1.0 , g= 9.8 , l0=1.0, gamma= 1.0):
    # Y' = K*Y + L 


    A = -gamma * np.identity( N )
    D = np.zeros( [N,N] )
    
    C = np.identity( N )

    B = -2. * np.identity( N )

    for i in range(1,N-1):
        B[i][i-1] = 1.
        B[i][i+1] = 1.

    B[0][0] = -1. 
    B[0][1] = 1.

    B[N-1][N-1] = -1.
    B[N-1][N-2] = 1.

    B = k/m * B

    X = np.block( [ A,B ] )
    Y = np.block( [ C,D ] )
    K = np.block( [ [X],[Y] ] )
    
    #making the constant vector

    L_vdot = g * np.ones([N])
    L_vdot[0] = g -l0/m  
    L_vdot[N-1] = g + l0/m
    
    L_xdot = np.zeros([N])

    L = np.block( [ L_vdot, L_xdot ] )

    return [ K , L ]




def make_initial_conditions( N=10, k=1.0, m=1.0, g=9.8, l0 = 1.0 ):
    v0 = np.zeros([N])

    x0 = np.zeros([N])

    x0[0] = 0.0
    x0[1] = (N-1)*m*g/k +l0

    for i in range(2,N):
        x0[i] = 2* x0[i-1] - x0[i-2] - m*g/k

    return (v0,x0)



def calculate(N,k,m,g,l0,gamma,t_i,t_f,t_delta):
    
    params = make_matrix(N=N, k=k, m=m, g=g, l0=l0, gamma=gamma)

    v0,x0 = make_initial_conditions(N=N,k=k, m=m, g=g, l0=l0)

    y0 = np.block( [v0,x0] )

    t = np.arange(t_i, t_f , t_delta)

    def derivative_ODE(y,t,params):
        K,L = params

        derivs = np.matmul( K , y ) + L

        return derivs

    psoln = odeint( derivative_ODE , y0, t, args=(params,))
    
    v = psoln[ :, 0:N ]
    x = psoln[ :, N:]
    
    cm = x.sum( axis = 1 ) / N 
    cm_simple = np.mean(x0) +  0.5*9.8*t*t 

    return (t,v,x,cm,cm_simple)

    

def main():
    
    N=30
    k=1.0
    m=1.0
    g=9.8
    l0=1.0
    gamma = 1.0
    t_i = 0.0
    t_f = 20.0
    t_delta = 0.05

    t, v, x, cm, cm_simple = calculate(
            N=N,
            k=k,
            m=m,
            g=g,
            l0=l0,
            gamma=gamma,
            t_i=t_i,
            t_f=t_f,
            t_delta=t_delta,
            )

    fig = plt.figure()
    
    #Test plot to compare motion of CM calculated both ways
    #plt.subplot(212)
    #plt.plot(t, cm , color ="green", label="CM DE")
    #plt.plot(t, cm_simple , color = "pink", label = "CM Simple")
    #plt.xlabel('time')
    #plt.ylabel('position')
    #plt.title('Test of Code with CM motion')
    #plt.legend()

    #Plot with CM, x1 and xN
    ax = plt.subplot(111)
    line_x1, =plt.plot( t , x[:,0] ,color = "red", label = r"$x_1$")
    line_x2, =plt.plot( t , x[:,1] , label = r"$x_2$")
    line_xN, = plt.plot( t, x[:,-1] , color = "blue", label = r"$x_N$")
    line_CM, = plt.plot(t, cm , color ="green", label=r"$CM$")
    plt.xlabel('time')
    plt.ylabel('position')
    plt.title('Calculated Positions')
    plt.legend()
    
    #making sliders
    plt.subplots_adjust(bottom=0.25)
    
    #N
    #plt.subplots_adjust(bottom=0.25)
    #ax_N = plt.axes([0.18,0.1,0.65,0.03])
    #slider_N = Slider(
    #        ax=ax_N,
    #        label='N',
    #        valmin = 10,
    #        valmax = 100,
    #        valinit = N,
    #        valstep = np.arange(10,100,1)
    #        )
    
    #k
    ax_k = plt.axes([0.18,0.02,0.65,0.02])
    slider_k = Slider(
            ax=ax_k,
            label='k',
            valmin = 1,
            valmax = 100,
            valinit = k,
            )
    
    #m
    ax_m = plt.axes([0.18,0.04,0.65,0.02])
    slider_m = Slider(
            ax=ax_m,
            label='m',
            valmin = 1,
            valmax = 100,
            valinit = m,
            )

    #l0
    ax_l0 = plt.axes([0.18,0.06,0.65,0.02])
    slider_l0 = Slider(
            ax=ax_l0,
            label=r'$l_0$',
            valmin = 1,
            valmax = 100,
            valinit = l0,
            )

    #t_f
    ax_t_f = plt.axes([0.18,0.08,0.65,0.02])
    slider_t_f = Slider(
            ax=ax_t_f,
            label=r'$t_f$',
            valmin = 1,
            valmax = 100,
            valinit = t_f,
            )

    #t_delta
    ax_t_delta = plt.axes([0.18,0.10,0.65,0.02])
    slider_t_delta = Slider(
            ax=ax_t_delta,
            label=r'$t_{\Delta}$',
            valmin = 0.005,
            valmax = 0.1,
            valinit = t_delta,
            )
    
    #gamma
    ax_gamma = plt.axes([0.18,0.12,0.65,0.02])
    slider_gamma = Slider(
            ax=ax_gamma,
            label=r'$\gamma$',
            valmin = 0.0,
            valmax = 10,
            valinit = gamma,
            )
    
    #Slider update function:
    def slider_update(val):
        t, v, x, cm, cm_simple = calculate(
                N=N,
                k=slider_k.val,
                m=slider_m.val,
                g=g,
                l0=slider_l0.val,
                gamma=slider_gamma.val,
                t_i=t_i,
                t_f=slider_t_f.val,
                t_delta=slider_t_delta.val,
                )
        
        
        x1 = x[:,0]
        x2 = x[:,1]
        xN = x[:,-1]

        line_x1.set_xdata(t)
        line_x1.set_ydata(x1)

        line_x2.set_xdata(t)
        line_x2.set_ydata(x2)

        line_xN.set_xdata(t)
        line_xN.set_ydata(xN)

        line_CM.set_xdata(t)
        line_CM.set_ydata(cm)
        
        xlim_lower = t[0]
        xlim_upper = t[-1]
        xlim_len = xlim_upper - xlim_lower
        xlim_lower = xlim_lower - xlim_len*0.05
        xlim_upper = xlim_upper + xlim_len*0.05

        ax.set_xlim(xlim_lower,xlim_upper)

        ylim_lower = np.min( np.block([x1,xN,cm, x2])  )
        ylim_upper = np.max( np.block([x1,xN,cm, x2]) )
        ylim_len = ylim_upper-ylim_lower
        ylim_lower = ylim_lower - ylim_len*0.05
        ylim_upper = ylim_upper + ylim_len*0.05

        ax.set_ylim(ylim_lower, ylim_upper)


        
    slider_k.on_changed(slider_update)
    slider_m.on_changed(slider_update)
    slider_l0.on_changed(slider_update)
    slider_t_f.on_changed(slider_update)
    slider_t_delta.on_changed(slider_update)
    slider_gamma.on_changed(slider_update)

    plt.show()

if __name__=="__main__":
    main()
