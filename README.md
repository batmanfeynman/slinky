# Slinky

This is a program that solves and plots the path of a slinky that has been dropped. Please refer to [this article](https://www.wired.com/2011/09/modeling-a-falling-slinky/) to understand the problem.

I will work on an explanation of the code in the future

This is distributed under the GNU General Public License v3 (or any later version) - see the accompanying COPYING file for more details.

# Running
1. Install a recent version of python. This was tested to run on python 3.9
2. (Optional) Create a virtual environment `python -m venv env`. 
Now activate the virtual environment with `source env/bin/activate`
3. Install dependencies. You can use the included requirements.txt file if you want `pip install -r requirements.txt`
4. Run the file. `python solve.py` or `./solve.py`
